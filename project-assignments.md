# Introduction

All the project assignments related to 2024 Summer Internship at VLEAD are captured here.

# Project List 

Each intern will be working on the projects assigned during the internship period. 

# Interns List

| S.No | Name of the Student | Email ID |Github Handle |Phone Number|
| :---: | --- | --- |--- | --- |
| 1. | Ansh Chablani|ansh.chablani@research.iiit.ac.in |anshium |7011503797 |
| 2. | Divyarajsinh Rajendrasinh Mahida|divukruti@gmail.com|Divyaraj-coder-create|7016517162|
| 3. | Gaurav Behera |gaurav.behera@research.iiit.ac.in |gaurav-behera| 7899435730|
| 4. | Nishita Kannan|nishita.kannan@outlook.com |112Nisha| 7207824159|
| 5. | Shruti Dhasmana |shruti23cc12@gmail.com|shruti-doon| 7987212695|


# Project Assignments

| S.No | Name of the Student | Project (Name & Repo Link) | Comments |
| :---: | --- | --- | --- |
| 1. |Ansh Chablani|[Progressive Web Application](https://github.com/virtual-labs/app-vlabs-pwa/blob/main/README.md) & [Service Worker Integration](https://github.com/virtual-labs/lib-service-worker-vlabs)| | |
| 2. |Divyarajsinh Rajendrasinh Mahida|[Question Bank Service](https://github.com/virtual-labs/svc-question-bank)| | |
| 3. |Gaurav Behera |[Systems-Virtual Labs AWS Infrastructure Modernization](https://docs.google.com/document/d/1v5U-uTN2ZhB964GB9795Xlvsk5TS6ckNGOH9UNyOnoY/edit#heading=h.5ye817ih4s6r) | | |
| 4. |Nishita Kannan |[Website Enhancement](https://github.com/virtual-labs/app-vlead-web) & [Workshop Reporting Tool](https://github.com/virtual-labs/app-outreach-tracker-web)| | |
| 5. |Shruti Dhasmana  |[VLSI, DLD, SpiceCode, Verilog Lab / Experiments](https://github.com/virtual-labs/engineers-forum/issues/929)  | | |

# Project Details
|S.No.|Name of the Project|Description|
| :---: | --- | --- |
|1.|Progressive Web Application|Ensure the presence of the PWA on Google Playstore.|
|2.|Service Worker Integration |1. Understand the first iteration of the Virtual Labs service worker.<br><br>2. Test the integration of Service Worker with the build script.<br><br>3. Test the deployment of the service worker with each experiment. <br><br>4. Test the functionality of adding experiment-specific features to the service worker. <br><br>5. Implement features such as caching and offline usage with the deployed service worker.<br><br>6. Develop backend systems for features like user interaction analytics before utilizing the service worker as their frontend.|
|3.|Question Bank Service|1. Establish four distinct roles: Administrator, Contributor, Question User, and Quiz Participant.<br><br>2. Develop a functionality within the Virtual Labs Deployment Tool, enabling the VLEAD Team to input the GitHub repository link. This capability will seamlessly integrate questions from pre-test and post-test .json files into the Question Bank database, ensuring proper categorization.<br><br>3. Implement a feature resembling the Bug Reporting Tool within the Virtual Labs interface. This feature, named "Explore More Questions," will prominently appear on both pre-test and post-test pages of the experiment. <br><br>4. Upon user interaction, it will redirect them to the Question Bank Service page with the role of Quiz Participant. Subsequently, users will be prompted to log in using their Gmail ID and presented with additional questions relevant to their topic of exploration. By default, users will engage with 10 questions and will receive evaluations based on their responses. To summarize,an interface for taking the quiz needs to be built.|
|4.|Systems-Virtual Labs AWS Infrastructure Modernization|Modernize and improve the Virtual Lab’s existing AWS cloud infrastructure. The project aims to achieve the following objectives: <br><br>1. Future-proof the cloud infrastructure: Ensure the infrastructure can adapt to evolving technologies and business needs. <br><br>2. Adopt serverless architecture: Migrate workloads away from traditional servers for improved scalability and cost efficiency.<br><br>3. Reduce management and operational costs: Streamline infrastructure management to minimize resource consumption and administrative overhead.<br><br>4. Enhance infrastructure visibility: Gain deeper insights into infrastructure performance and resource utilization.<br><br> 5. Simplify infrastructure management: Facilitate easier provisioning, deployment, and maintenance of infrastructure components.|
|5.|Website Enhancements|1. Enhance the VLEAD Website:<br>- Showcase VLEAD Team<br>- Enable VLEAD report publishing, and progress tracking from GitHub.<br>- Integrate features for easy content addition.<br>- Ensure user-friendliness for various stakeholders:<br> a. Users: Enhance usability for smooth navigation and access.<br> b. Content Creators: Provide access to bug repository for efficient issue tracking and resolution.<br> c. VLEAD System Administrators: Grant access to Lab Deployment Tool for seamless management. <br> d. Nodal Centers: Enable access to workshop reporting Tool for streamlined reporting processes.<br><br>2. Test Website Enhancements:<br>- Conduct thorough testing to identify and rectify any bugs or issues.<br>- Ensure compatibility across different browsers and devices.<br>- Validate functionality and user experience to meet specified requirements. <br><br>3. Deploy Enhanced Website:<br>- Coordinate with relevant teams to ensure smooth deployment.<br>- Monitor deployment process and address any issues promptly.<br>- Conduct post-deployment testing to ensure functionality and performance in the live environment.|
|6.|Workshop Reporting Tool|1. Implement Responsive Design:Update the HTML and CSS code to incorporate responsive design principles.Ensure images and media adapt appropriately to different devices.<br><br>2. Test Responsiveness:Conduct thorough testing across various devices (laptops, and smartphones).Test the Workshop Reporting Tool on different browsers to ensure consistent performance. Verify that all elements resize and rearrange correctly based on screen size.<br><br>3. Finalize and Deploy: Coordinate with the development team to deploy the updated Workshop Reporting Tool. Monitor the tool post-deployment to address any issues that may arise.|
|7.|DLD,VLSI, Spice code and Verilog Labs| Review, test, create issues for bugs and fix all the 35 experiments across these 4 lab. |

# Internship Agenda

## General Instructions 

### Realization of Projects

Adopt [Agile](https://www.atlassian.com/agile) software development principles and adhere to the [Scrum](https://www.atlassian.com/agile/scrum) methodology for the incremental development of functional software. 
1. Set up/Ensure a dedicated GitHub repository for each project to be worked on.
2. Decompose assigned projects into smaller, manageable tasks.
3. Further divide tasks into subtasks for detailed planning and execution.
4. Create GitHub issues within the corresponding project repository for each task and subtask.
5. Assign yourself to each issue to take ownership of its completion.
6. Categorize all issues under the project label "VLEAD Tasks" to organize and track progress.
7. Set the status of each issue to "In Progress" to indicate active development.
8. If preemptively creating issues for all subtasks, designate their status as "To Do" until work begins.
9. Appropriately label issues as "Enhancements" or "Bugs" based on their nature and priority.
10.Set the status of issue to "Closed" to once the task has been completed. 

### Communication

Every discussion about the project will be through issues.  Mails are not used for discussion.  Any informal communication will be through Slack - [2024-summer-interns]((https://vleadworkspace.slack.com/C070LNZBCVC)) channel.

### Things done at the weekly meeting

1. A presentation of the previous week's plan, accomplished tasks and backlogs. 
2. Discussion of code, documents and a demonstration.
2. Review of pull/merge request if any.
3. Task planning for the next week.  The students will be responsible for taking the meeting notes.  The meeting notes may follow the structure as shown below. 
    - Agenda
    - Discussion Points
    - Action Items

### Work logs 

The work logs of each intern will need to be pushed to the directory shared in the [On-boarding document](https://gitlab.com/vlead-projects/summer-internship-2024/-/blob/main/on-boarding.md).

### Team Meetings

Team meetings will be held every Wednesday at 10:30 AM with a slotted time of 20 min per intern.

### Slack Channel

Communication will be through [Slack](https://vleadworkspace.slack.com) during the internship period. If you need any guidance regarding anything, just post it on [2024-summer-interns](https://vleadworkspace.slack.com/archives/C070LNZBCVC) channel. To get access to this channel send out a mail to *ravi.kiran@vlabs.ac.in*.  Please be available at all times on [2024-summer-interns](https://vleadworkspace.slack.com/archives/C070LNZBCVC) channel on [Slack]().





